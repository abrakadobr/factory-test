
const F = require('./fac')

const AbstractModule = require('./mdl')
const FacMix = require('./facm')

class Core extends F.extends(AbstractModule)
{
  constructor(p)
  {
    super(p)
    console.log('core object constructor',this._parent)
  }

  go()
  {
    //super.go()
    console.log('core object go',this)
    let m = this.M('x')
    console.log('core object M',m)
  }
}

module.exports = F.register(Core)
