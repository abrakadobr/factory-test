const F = require('./fac')

class MyMix {
  constructor(...args)
  {
    this._opopop = args
    console.log('MyMix constructor',args,this)
  }
  M2(code)
  {
    console.log('MyMix M2',code,this)
  }
}

F.mix('AbstractModule',MyMix)

module.exports = F.mix('AbstractModule',{
  M(code) {
    console.log('mix M',code,this)
    this.M2(this._opopop)
    return this._opopop[code]
  }
})
