const F = require('./fac')

class AbstractModule extends F.extends('AbstractObject')
{
  constructor(p)
  {
    super(p)
    console.log('abstract module constructor',this._parent)
  }

  go()
  {
    console.log('abstract module object go')
  }
}

module.exports = F.register(AbstractModule)
