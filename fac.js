const AbstractObject = require('./obj')

var __fac_classes = { AbstractObject }
var __fac_mixins = []

function __fac_register(Cls)
{
  __fac_classes[Cls.name] = Cls
  return Cls
}

function __fac_copyProps(target, source)
{ 
  // this function copies all properties and symbols, filtering out some special ones
  Object.getOwnPropertyNames(source)
    .concat(Object.getOwnPropertySymbols(source))
    .forEach((prop) => {
      if (!prop.match(/^(?:constructor|prototype|arguments|caller|name|bind|call|apply|toString|length)$/))
        Object.defineProperty(target, prop, Object.getOwnPropertyDescriptor(source, prop));
    })
}


function __fac_mix(mixFor,mixin)
{
  let mfor = ['']
  if (Array.isArray(mixFor)&&typeof mixin === 'object')
    mfor = mixFor
  if (typeof mixFor === 'string'&&(typeof mixin === 'object' || typeof mixin === 'function'))
    mfor = [mixFor]
  if (typeof mixin === 'undefined' && (typeof mixFor === 'object' || typeof mixFor === 'function'))
    mixin = mixFor
  mfor.forEach(mf=>{
    __fac_mixins.push({mf,mixin})
  })
  return mixin
}

function __fac_class(cls)
{
  let ret = null
  if (typeof cls === 'string')
    ret = __fac_classes[cls]
  if (typeof cls === 'function')
  {
    if (!!__fac_classes[cls.name])
      ret = __fac_classes[cls.name]
    else
      ret = __fac_register(cls)
  }
  if (!ret) return ret
  ret.__mixins = []
  __fac_mixins.forEach((mix) => {
    if (mix.mf!==''&&mix.mf!==ret.name)
      return
    if (typeof mix.mixin === 'object')
      __fac_copyProps(ret.prototype,mix.mixin);
    else
      ret.__mixins.push(mix)
  });
  return ret
}

function __fac_extends(...classes)
{
  const baseClass = __fac_class(classes.shift())
  const mixins = [...classes.map(x=>__fac_class(x)),...baseClass.__mixins.map(x=>x.mixin)]
  class base extends baseClass {
    constructor (...args) {
      super(...args);
      //console.log('baseClass constructor',this)
      mixins.forEach((mixin) => {
        //console.log('mix with',mixin)
        let nm = (new mixin(...args))
        __fac_copyProps(this,nm);
        //console.log('copy extend class',this,nm)
        //nm.call(this,...args)
      });
    }
  }
  mixins.forEach((mixin) => { // outside contructor() to allow aggregation(A,B,C).staticFunction() to be called etc.
    __fac_copyProps(base.prototype, mixin.prototype);
    __fac_copyProps(base, mixin);
  });
  return base;
}

var __fac_app = null

var F = function(coreClass,...startArgs) {
  if (__fac_app) return __fac_app
  const Cls = __fac_class(coreClass)
  if (!Cls) return null
  __fac_app = new Cls(...startArgs)
  return __fac_app
}

F.mix = __fac_mix
F.class = __fac_class
F.extends = __fac_extends
F.register = __fac_register

module.exports = F

/*

class Fac
{
  constructor(coreCls)
  {
    let core = Fac.class(coreCls)
  }

  static mix(mixFor,mixin)
  static class(cls)
  static copyProps(target, source)
  static extends(...classes)
  static register(Cls)
}

//var F = new Fac()

module.exports = Fac
*/
